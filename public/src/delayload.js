function isInView(element) {
  const rect = element.getBoundingClientRect();
  const buffer = 10; // Buffer in pixels

  return (
    rect.top <
      (window.innerHeight || document.documentElement.clientHeight) + buffer &&
    rect.bottom > -buffer &&
    rect.left <
      (window.innerWidth || document.documentElement.clientWidth) + buffer &&
    rect.right > -buffer
  );
}

function debounce(func, wait) {
  let timeout;
  return function (...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => func.apply(this, args), wait);
  };
}

const handleScroll = debounce(() => {
  const elements = document.querySelectorAll(".content");
  elements.forEach((element) => {
    if (isInView(element)) {
      element.classList.add("visible");
    } else {
      element.classList.remove("visible");
    }
  });
}, 50); // Adjust the debounce wait time as needed

window.addEventListener("scroll", handleScroll);
window.addEventListener("load", handleScroll);

const observer = new IntersectionObserver(
  (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        entry.target.classList.add('fade-in-active');
      } else {
        entry.target.classList.remove('fade-in-active');
      }
    });
  },
  { threshold: 0.4 } // Adjusted to 0.4 to trigger earlier, but still noticeable
);

const banners = document.querySelectorAll('.banner-1-container, .banner-2-container, .banner-3-container, .banner-4-container');

banners.forEach((banner) => {
  observer.observe(banner);
});



