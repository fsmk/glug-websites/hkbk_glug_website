let slider = tns({
  container: ".the-team-behind-slider-wrapper",
  items: 1,
  // startIndex: 3,
  autoplay: true,
  autoplayTimeout: 3000,
  navPosition: "bottom",
  autoplayHoverPause: true,
  navContainer: ".the-team-behind-nav-container",
  autoplayButtonOutput: false,
  mouseDrag: true,
  controls: false,
  responsive: {
    350: {
      items: 1,
      controls: false,
      edgePadding: 50,
      center: true,
    },
    500: {
      items: 1,
    },
    600: {
      items: 2
    },
    768: {
      items: 1
    },
    1600: {
      items: 3
    },
    1400: {
      items: 2.5
    },
    1100:{
      items: 2
    }
  },
  swipeAngle: false,
  speed: 500,
});
